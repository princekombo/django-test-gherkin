(function() {
    try {
        const isHidden = document.querySelector('.hidden');
        if (isHidden) {
            const contentWarning = {
                display: true,
                content: document.getElementById('article-disclaimer').innerHTML
            }

            alertify.confirm('Attention !', contentWarning.content, () => {
                document.querySelectorAll('.hidden img').forEach(e => { e.style.display = 'block' });
            }, () => {});
        }
    }
    catch(err) {}
})();

(function() {
    const burger = document.getElementsByClassName('burger')[0];
    const menu = document.getElementById(burger.dataset.target);
    burger.addEventListener('click', function() {
        burger.classList.toggle('is-active');
        menu.classList.toggle('is-active');
    });

})();