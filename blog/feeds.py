from django.contrib.syndication.views import Feed
from django.urls import reverse
from django.shortcuts import get_object_or_404

from blog.models import Article, Comment, Blog

class LatestArticlesFeed(Feed):
    blog = get_object_or_404(Blog, pk=1)
    title = blog.name
    description = 'Last posts'
    link = 'feeds/'

    def items(self):
        return Article.objects.filter(published=True).order_by('-publication_date')[:10]
    
    def item_title(self, item):
        return item.title
    
    def item_description(self, item):
        return item.content[:400]
    
    def item_link(self, item):
        return reverse('blog:article', args=[item.slug])

class UnvalidedCommentsFeed(Feed):
    blog = get_object_or_404(Blog, pk=1)
    title = '{0} - unvalided comments'.format(blog.name,)
    description = 'Last comments which are unvalided'
    link = 'feeds/'

    def items(self):
        return Comment.objects.filter(valided=False).order_by('-publication_date')
    
    def item_title(self, item):
        return item.username
    
    def item_description(self, item):
        return item.content
    
    def item_link(self, item):
        return reverse('blog:index')

class ValidedCommentsFeed(Feed):
    blog = get_object_or_404(Blog, pk=1)
    title = '{0} - comments'.format(blog.name,)
    description = '{0} - commentaires'.format(blog.name,)
    link = 'feeds/'

    def items(self):
        return Comment.objects.filter(valided=True).order_by('-publication_date')[:50]
    
    def item_title(self, item):
        return item.username
    
    def item_description(self, item):
        return item.content
    
    def item_link(self, item):
        return reverse('blog:article', args=[item.article.slug])
